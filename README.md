# GitLab CI with Writerside

This repository contains an example GitLab job configuration that builds a Writerside project.

Use the image `registry.jetbrains.team/p/writerside/builder/writerside-builder:2.1.931` and call
```
/opt/builder/bin/idea.sh helpbuilderinspect -source-dir . -product $PRODUCT -output-dir artifacts/ || true
```

The `$PRODUCT` should be `name_of_module` / `product_id` so in our example it is `sample_module/wt`.

This will generate a zip archive with the built web pages. The name of the archive is `webHelpXX2-all.zip` where `XX` gets replaced by the product id in caps.

See the [.gitlab-ci.yml](.gitlab-ci.yml) file for an example in full context.
